FROM ubuntu:16.04

# Install https driver first
RUN apt-get -y update
RUN apt-get -y install apt-transport-https ca-certificates
RUN sh -c 'echo "deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/dotnet-release/ xenial main" > /etc/apt/sources.list.d/dotnetdev.list'
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 417A0893

# Install dotnet
RUN apt-get -y update
RUN apt-get -y --no-install-recommends install dotnet-dev-1.0.1 curl

# Install 7.x node
RUN curl -sL https://deb.nodesource.com/setup_7.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt-get -y install nodejs build-essential
RUN apt-get -y install git

# Initialize dotnet
WORKDIR /app/
RUN dotnet new console && dotnet restore
CMD ["dotnet", "run"]